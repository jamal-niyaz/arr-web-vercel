import Head from "next/head"
import Clients from '../components/main/Clients'
import Cta from '../components/main/Cta'
import Features from '../components/main/Features'
import Hero from '../components/main/Hero'
import Testimonials from '../components/main/Testimonials'

export default function Home() {
  return (
    <>
      <Head>
        <title>Home - ARR Global Services LLC</title>
      </Head>
      <div>
        {/* <Slider /> */}
        <Hero />
        <Features />
        <Clients />
        <Testimonials />
        <Cta />
      </div>
    </>
  )
}
