import Swiper from 'react-id-swiper';

export default function Slider() {
    const params = {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        }
    }

    return (
        <Swiper {...params}>
            <div className="swiper-slide p-4">
                <div className="flex flex-col rounded shadow overflow-hidden">
                    <div className="flex-shrink-0">
                        <img src='/images/more/9.jpg' alt='slide 1' />
                    </div>
                </div>
            </div>
            <div className="swiper-slide p-4">
                <div className="flex flex-col rounded shadow overflow-hidden">
                    <div className="flex-shrink-0">
                        <img src='/images/more/3.jpg' alt='slide 2' />
                    </div>
                </div>
            </div>
            <div className="swiper-slide p-4">
                <div className="flex flex-col rounded shadow overflow-hidden">
                    <div className="flex-shrink-0">
                        <img src='/images/more/16.jpg' alt='slide 3' />
                    </div>
                </div>
            </div>
        </Swiper>
    );
};