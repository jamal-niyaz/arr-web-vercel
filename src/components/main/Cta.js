import Link from 'next/link'

export default function Cta() {
    return (
        <div className="bg-gray-50">
            <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
                <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                    <span className="block">Ready to dive in?</span>
                    <span className="block text-blue-800">Work With Expertise</span>
                </h2>
                <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
                    <div className="inline-flex rounded-md shadow">
                        <a
                            href="https://wa.me/971553979456?text=I'm%20interested%20in%20your%20services,%20shall%20we%20discuss"
                            target="_blank"
                            className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-700"
                        >Let's Call</a>
                    </div>
                    <div className="ml-3 inline-flex rounded-md shadow">
                        <Link href="/services">
                        <a className="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-blue-600 bg-white hover:bg-blue-50"
                        >More Services</a>
                    </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
