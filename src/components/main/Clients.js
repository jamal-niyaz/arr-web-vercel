export default function Clients() {
    return (
        <section class="bg-white pt-7">
            <div class="container px-8 mx-auto sm:px-12 lg:px-20">
                <h2 className="text-base text-center mb-7    text-blue-800 font-semibold tracking-wide uppercase">Trusted by top-leading companies.</h2>
                <div class="flex grid items-center justify-center grid-cols-4 grid-cols-12 gap-y-8">
                    <div class="flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2">
                        <img src="/images/clients/dubai-hills.png" alt="Dubai Hills" class="block object-contain h-36" />
                    </div>
                    <div class="flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2">
                        <img src="/images/clients/ecc.jpg" alt="Ecc" class="block object-contain h-36" />
                    </div>
                    <div class="flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2">
                        <img src="/images/clients/jumeirah.jpg" alt="jumeirah" class="block object-contain h-36" />
                    </div>
                    <div class="flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2">
                        <img src="/images/clients/dubai-hills.png" alt="Dubai Hills" class="block object-contain h-36" />
                    </div>
                    <div class="flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2">
                        <img src="/images/clients/ecc.jpg" alt="Ecc" class="block object-contain h-36" />
                    </div>
                    <div class="flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2">
                        <img src="/images/clients/jumeirah.jpg" alt="jumeirah" class="block object-contain h-36" />
                    </div>
                </div>
            </div>
        </section>

    )
}
