// import { useState, useEffect } from "react";
import Head from "next/head";
import Header from "../shared/Header";
import Footer from "../shared/Footer";

const SiteLayout = ({
    children,
    title = "Arrglobalservices.com",
    description = "Arrglobalservices",
    keywords = "Arrglobalservices",
    type = 'website',
    url = '/',
    image = 'favicon.ico',
    origin = '',
}) => {
    return (
        <div className="h-screen">
            <Head>
                <title>{title}</title>
                <meta charSet="utf-8" />
                <meta name="robots" content="index, follow" />
                <meta name="description" content={description} />
                <meta name="keywords" content={keywords} />
                <meta property="twitter:image:src" content={`${origin}${image}?v=${Math.floor(Date.now() / 100)}`} />
                <meta property="twitter:card" content="summary" />
                <meta property="twitter:url" content={url} />
                <meta property="twitter:title" content={title} />
                <meta property="twitter:description" content={description} />
                <meta property="og:image" content={`${origin}${image}?v=${Math.floor(Date.now() / 100)}`} />
                <meta property="og:site_name" content={url} />
                <meta property="og:type" content={type} />
                <meta property="og:title" content={title} />
                <meta property="og:url" content={url} />
                <meta property="og:description" content={description} />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <link rel="icon" href={image} />
            </Head>

            <Header />
            <div>
                {children}
            </div>
            <Footer />
        </div>
    )
}

export default SiteLayout;